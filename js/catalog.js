function CatalogTable() {
  var catalogTableEl = $('#catalog-table');
  var headerElms  = $('.catalog-table__item_title');
  var mobileOrderSelectEl = $('.catalog-mobile-sort__select-clm');
  var mobileDirOrderSelectEl = $('.catalog-mobile-sort__orderdir-clm');
  //var clmnsCount;
  var activeClmnNum;
  var activeClmnOrderDir;

  init();

  function init() {
    table = catalogTableEl.DataTable({
      "paging":   false,
      "info":     false,
      "language":{
        "search":"",
        "searchPlaceholder":"Поиск...",
        "zeroRecords":"Товров по вашему запросу не найдено",
      },
      "oSearch": {"sSearch": window.catalogSearch}
    });

    headerElms.each(function(i, obj) {
      obj.addEventListener("click",function(){
        setOrderToClm(i);
      })
    });

    mobileOrderSelectEl.change(function(){
      var clmNum = mobileOrderSelectEl.val();
      setOrderToClm(clmNum);
    })

    mobileDirOrderSelectEl.change(function(){
      var clmNum = mobileOrderSelectEl.val();
      setOrderToClm(clmNum);
    });

    activeClmnNum = 0;
    activeClmnOrderDir = "asc";
    setOrder();
    //clmnsCount=table[0].rows[0].cells.length;
  }

  function setOrder() {    
    table
      .order( [activeClmnNum, activeClmnOrderDir ] )
      .draw();
    headerElms.removeClass('sorting-asc sorting-desc');
    headerElms.eq(activeClmnNum).addClass('sorting-'+activeClmnOrderDir);
    mobileOrderSelectEl.val(activeClmnNum);
  }

  function changeActiveClmOrderDir() {

    if (activeClmnOrderDir=="asc") {
      activeClmnOrderDir="desc";
    }
    else {
      activeClmnOrderDir = "asc";
    }
  }

  function setOrderToClm(clmNum){
    if (activeClmnNum==clmNum) {
      changeActiveClmOrderDir();
    }
    else {
      activeClmnNum = clmNum;
    }
    
    setOrder();
  }
    
}

function FixedHeader() {
  var catalogHead = $('.catalog__fixed-head'); 
  var catalogMobileHeader = $('.catalog-mobile-header');
  var searchEl = $('.dataTables_filter');

  init();

  function init() {
    $(window).scroll(onScrollCahnge);
  }

  function onScrollCahnge() {
    var pos = $(window).scrollTop();  
   
    if (pos>250) {
        catalogHead.addClass('catalog__fixed-head_fixed');
        searchEl.addClass('dataTables_filter_fixed-head');
        catalogMobileHeader.addClass('catalog-mobile-header_fixed');
    }
    else {
        catalogHead.removeClass('catalog__fixed-head_fixed');
        searchEl.removeClass('dataTables_filter_fixed-head');
        catalogMobileHeader.removeClass('catalog-mobile-header_fixed');
    }
  }  
}


$(document).ready(function() {
  var catalog = new CatalogTable();
  var fh = new FixedHeader();
});

