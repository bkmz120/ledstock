$(document).ready(function() {
	window.mMenu = new MMenu;
});

function MMenu() {
	var menu = document.querySelector('.mobile-menu');
	var menuBtn = document.querySelector('.mobile-menu-btn');
	var content = document.querySelector('.sitewrap');
	var contentInner = document.querySelector('.sitewrap__inner');
	var MENU_WIDTH = 240;

	this.show = show;
	this.close = close;

	function show(){
		//var winWidth = window.innerWidth || document.body.clientWidth;
		//var contentWidth = winWidth - MENU_WIDTH;
		//content.style.width = contentWidth + 'px';
		//contentInner.style.width = winWidth + 'px';
		console.log(menu);
		menuBtn.className += " mobile-menu-btn_open";
		menu.className += " mobile-menu_visible";
		content.className += " sitewrap_menu-open";
		setTimeout(function(){
			content.addEventListener('click',close);
		},500);				
	}

	function close(){		
		//content.style.width = "100%";
		//contentInner.style.width = "auto";
		content.removeEventListener('click',close);
		content.className = content.className.replace(new RegExp('(?:^|\\s)'+ 'sitewrap_menu-open' + '(?:\\s|$)'), ' ');
		menuBtn.className = menuBtn.className.replace(new RegExp('(?:^|\\s)'+ 'mobile-menu-btn_open' + '(?:\\s|$)'), ' ');
		menu.className = menu.className.replace(new RegExp('(?:^|\\s)'+ 'mobile-menu_visible' + '(?:\\s|$)'), ' ');
		
	}
}
$(document).ready(function() {
  conctactForm = new Form('.contacts__form','/formApi/send');
  window.popupOrderForm = new Form('.order-popup','/formApi/send');
});

function Form(_formSelector,_apiUrl) {
  var formEl;
  var inputNameEl,inputEmailEl,inputPhoneEl,inputConfirmEl,productNameEl;
  var wrapNameEl,wrapEmailEl,wrapPhoneEl,wrapConfirmEl;
  var sendBtnEl;

  var validateStatus;

  this.send = send;
  this.clear = clear;

  init();

  function init() {
    formEl = $(_formSelector);
    productNameEl = $(_formSelector + ' .productName');
    inputNameEl = $(_formSelector + ' .formWrapName .formInput');
    inputEmailEl = $(_formSelector + ' .formWrapEmail .formInput');
    inputPhoneEl = $(_formSelector + ' .formWrapPhone .formInput');
    inputConfirmEl = $(_formSelector + ' .formWrapConfirm .formInput');

    wrapNameEl = $(_formSelector + ' .formWrapName');
    wrapEmailEl = $(_formSelector + ' .formWrapEmail');
    wrapPhoneEl = $(_formSelector + ' .formWrapPhone');
    wrapConfirmEl = $(_formSelector + ' .formWrapConfirm');
    sendBtnEl = $(_formSelector + ' .formSendBtn');
    sendBtnEl.on("click",validate);
    inputPhoneEl.inputmask({"mask": "+7 (999) 999-9999"});
  }

  function validate() {
    validateStatus = {
      name:true,
      email:true,
      phone:true,
      confirm:true,
    }

    values = {
      name:inputNameEl.val(),
      email:inputEmailEl.val(),
      phone:inputPhoneEl.val(),
      confirm:inputConfirmEl.prop("checked"),
    }
    
    var validateTotal = true;
    if (values.name=="") {
      validateStatus.name =  false;
      validateTotal = false;
    }
    if (values.email=="") {
      validateStatus.email =  false;
      validateTotal = false;
    }
    
    if (values.phone=="" || !inputPhoneEl.inputmask("isComplete")) {
      validateStatus.phone =  false;
      validateTotal = false;
    }

    if (validateTotal==true && values.confirm==false) {
      validateStatus.confirm = false;
      validateTotal = false;
    }

    updateValidate(validateStatus);

    if (validateTotal) {
      formEl.addClass('sending');
      if (productNameEl.length!=0) {
        values.productName = productNameEl.text();
      }
      send(values);
    }   
  } 

  function updateValidate(status) {

    if (!status.name) {
      wrapNameEl.addClass('error');
    }
    else {
      wrapNameEl.removeClass('error');
    }
    if (!status.email) {
      wrapEmailEl.addClass('error');
    }
    else {
      wrapEmailEl.removeClass('error');
    }
    if (!status.phone) {
      wrapPhoneEl.addClass('error');
    }
    else {
      wrapPhoneEl.removeClass('error');
    }

    if (!status.confirm) {
      wrapConfirmEl.addClass('error');
    }
    else {
      wrapConfirmEl.removeClass('error');
    }
  }

  function send(values) {    
    var data = generateData(values);
    values.date = data;
    $.ajax({
      type: "POST",
      url: _apiUrl,
      data: values,
      success: sendCallback,
      dataType: "json"
    });
  }

  function sendCallback(data) {
    formEl.removeClass('sending');
    if (data.status) {
      formEl.addClass('success');
    }
    else {
      formEl.addClass('fail');
    }
  }

  function generateData(values) {
    //var result =  17*(1373 + values.name.length + values.email.length + values.phone.length);
    var date = new Date(); 
    var result = 4721*date.getSeconds();
    return result;
  }

  function clear() {
    inputNameEl.val('');
    inputEmailEl.val('');
    inputPhoneEl.val('');
    validateStatus = {
      name:true,
      email:true,
      phone:true,
      confirm:true,
    }
    updateValidate(validateStatus);
    formEl.removeClass('sending');
    formEl.removeClass('success');
    formEl.removeClass('fail');
  }

}
$(document).ready(function() {
	$('.slider').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		fade:true,
		arrows:false,
		dots:true,
		dotsClass:"slider__dots",
		autoplay:true,
		autoplaySpeed:6000,
	});
});

function CatalogTable() {
  var catalogTableEl = $('#catalog-table');
  var headerElms  = $('.catalog-table__item_title');
  var mobileOrderSelectEl = $('.catalog-mobile-sort__select-clm');
  var mobileDirOrderSelectEl = $('.catalog-mobile-sort__orderdir-clm');
  //var clmnsCount;
  var activeClmnNum;
  var activeClmnOrderDir;

  init();

  function init() {
    table = catalogTableEl.DataTable({
      "paging":   false,
      "info":     false,
      "language":{
        "search":"",
        "searchPlaceholder":"Поиск...",
        "zeroRecords":"Товров по вашему запросу не найдено",
      },
      "oSearch": {"sSearch": window.catalogSearch}
    });

    headerElms.each(function(i, obj) {
      obj.addEventListener("click",function(){
        setOrderToClm(i);
      })
    });

    mobileOrderSelectEl.change(function(){
      var clmNum = mobileOrderSelectEl.val();
      setOrderToClm(clmNum);
    })

    mobileDirOrderSelectEl.change(function(){
      var clmNum = mobileOrderSelectEl.val();
      setOrderToClm(clmNum);
    });

    activeClmnNum = 0;
    activeClmnOrderDir = "asc";
    setOrder();
    //clmnsCount=table[0].rows[0].cells.length;
  }

  function setOrder() {    
    table
      .order( [activeClmnNum, activeClmnOrderDir ] )
      .draw();
    headerElms.removeClass('sorting-asc sorting-desc');
    headerElms.eq(activeClmnNum).addClass('sorting-'+activeClmnOrderDir);
    mobileOrderSelectEl.val(activeClmnNum);
  }

  function changeActiveClmOrderDir() {

    if (activeClmnOrderDir=="asc") {
      activeClmnOrderDir="desc";
    }
    else {
      activeClmnOrderDir = "asc";
    }
  }

  function setOrderToClm(clmNum){
    if (activeClmnNum==clmNum) {
      changeActiveClmOrderDir();
    }
    else {
      activeClmnNum = clmNum;
    }
    
    setOrder();
  }
    
}

function FixedHeader() {
  var catalogHead = $('.catalog__fixed-head'); 
  var catalogMobileHeader = $('.catalog-mobile-header');
  var searchEl = $('.dataTables_filter');

  init();

  function init() {
    $(window).scroll(onScrollCahnge);
  }

  function onScrollCahnge() {
    var pos = $(window).scrollTop();  
   
    if (pos>250) {
        catalogHead.addClass('catalog__fixed-head_fixed');
        searchEl.addClass('dataTables_filter_fixed-head');
        catalogMobileHeader.addClass('catalog-mobile-header_fixed');
    }
    else {
        catalogHead.removeClass('catalog__fixed-head_fixed');
        searchEl.removeClass('dataTables_filter_fixed-head');
        catalogMobileHeader.removeClass('catalog-mobile-header_fixed');
    }
  }  
}


$(document).ready(function() {
  var catalog = new CatalogTable();
  var fh = new FixedHeader();
});


$(document).ready(function() {
	window.orderPopup = new OrderPopup;
});

function OrderPopup() {
	var popup = $('.order-popup');
	var bg = $('.order-popup__bg');
	var prodNameEl = $('.order-popup__prod-name');
	var prodImgEl = $('.order-popup__prod-img');
	
	this.show = show;
	this.close = close;
	this.send = send;

	function show(prodName,prodImgSrc) {
		window.popupOrderForm.clear();
		var top = $(document).scrollTop() + 50;
		popup.css('top',top);
		prodNameEl.text(prodName);
		prodImgEl.attr('src', prodImgSrc);
		popup.addClass('order-popup_visible');
		bg.addClass('order-popup__bg_visible');
		//$('body').css('overflow','hidden');
	}

	function close() {
		popup.removeClass('order-popup_visible');
		bg.removeClass('order-popup__bg_visible');
		$('body').css('overflow','visible');
	}

	function send() {
		close();
	}
}