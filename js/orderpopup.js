$(document).ready(function() {
	window.orderPopup = new OrderPopup;
});

function OrderPopup() {
	var popup = $('.order-popup');
	var bg = $('.order-popup__bg');
	var prodNameEl = $('.order-popup__prod-name');
	var prodImgEl = $('.order-popup__prod-img');
	
	this.show = show;
	this.close = close;
	this.send = send;

	function show(prodName,prodImgSrc) {
		window.popupOrderForm.clear();
		var top = $(document).scrollTop() + 50;
		popup.css('top',top);
		prodNameEl.text(prodName);
		prodImgEl.attr('src', prodImgSrc);
		popup.addClass('order-popup_visible');
		bg.addClass('order-popup__bg_visible');
		//$('body').css('overflow','hidden');
	}

	function close() {
		popup.removeClass('order-popup_visible');
		bg.removeClass('order-popup__bg_visible');
		$('body').css('overflow','visible');
	}

	function send() {
		close();
	}
}