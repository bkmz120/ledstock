<?php

class ImportException extends Exception
{
	public $errorRowNum;
    
    public function __construct($errorRowNum, $message, $code = 0, Exception $previous = null) {
      	$this->errorRowNum = $errorRowNum;
        parent::__construct($message, $code, $previous);
    }

}