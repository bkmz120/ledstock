<?php

class MainController extends CController
{
	public $layout='/layouts/layout';
	public $defaultAction = 'index';
	public $activeMenuLink = "index";

	public function actionIndex() {
		$catalogManager = new CatalogManager;
		$leds = $catalogManager->getLeds(12,true,true);

		$this->activeMenuLink = "index";
		$this->render('index',array('products'=>$leds));
	}

	public function actionCatalog($search='') {	
		$catalogManager = new CatalogManager;
		
		$products = $catalogManager->getLeds();
		$catalogTemplate = "catalogLeds";
		$catalogTitle = "Светодиодные ленты";
				
		if (!$typeNotFound) {
			$this->activeMenuLink = "catalog";
			$this->render($catalogTemplate,array('products'=>$products,
										  'catalogTitle'=>$catalogTitle,
										  'search'=>$search,
										));
		}		
	}

	public function actionContacts() {
		$this->activeMenuLink = "contacts";
		$this->render('contacts');
	}

	public function actionTest() {
		$this->render('test');
	}


	public function actionError() {
		if($error=Yii::app()->errorHandler->error)
		{
			$this->render('error', $error);
		}
	}
}
