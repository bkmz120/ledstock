<?php

/**
 * This is the model class for table "leds".
 *
 * The followings are the available columns in table 'leds':
 * @property integer $id
 * @property string $name
 * @property string $density
 * @property string $protection
 * @property string $price
 * @property string $power
 * @property string $color
 * @property integer $stock
 * @property string $photo1
 * @property string $photo1_thmb
 * @property string $photo2
 * @property string $photo2_thmb
 */
class Leds extends CActiveRecord
{
	public $photo1Url;
	public $photo1ThmbUrl;

	public $photo2Url;
	public $photo2ThmbUrl;

	public $photo3Url;


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'leds';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, density, protection, price, power, color, stock', 'required'),
			array(' stock', 'numerical', 'integerOnly'=>true),
			array('name, density, protection, power, color,price, photo1, photo1_thmb, photo2, photo2_thmb,photo3', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, density, protection, price, power, color, stock, photo1, photo1_thmb, photo2, photo2_thmb,photo3', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'density' => 'Density',
			'protection' => 'Protection',
			'price' => 'Price',
			'power' => 'Power',
			'color' => 'Color',
			'stock' => 'Stock',
			'photo1' => 'Photo1',
			'photo1_thmb' => 'Photo1 Thmb',
			'photo2' => 'Photo2',
			'photo2_thmb' => 'Photo2 Thmb',
		);
	}

	public function afterFind()
    {
    	$this->photo1Url = CatalogManager::LEDS_IMG_FOLDER_URL.$this->photo1;
        $this->photo1ThmbUrl = CatalogManager::LEDS_IMG_FOLDER_URL.$this->photo1_thmb;

        $this->photo2Url = CatalogManager::LEDS_IMG_FOLDER_URL.$this->photo2;
        $this->photo2ThmbUrl = CatalogManager::LEDS_IMG_FOLDER_URL.$this->photo2_thmb;

        $this->photo3Url = CatalogManager::LEDS_IMG_FOLDER_URL.$this->photo3;
        return parent::afterFind();
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('density',$this->density,true);
		$criteria->compare('protection',$this->protection,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('power',$this->power,true);
		$criteria->compare('color',$this->color,true);
		$criteria->compare('stock',$this->stock);
		$criteria->compare('photo1',$this->photo1,true);
		$criteria->compare('photo1_thmb',$this->photo1_thmb,true);
		$criteria->compare('photo2',$this->photo2,true);
		$criteria->compare('photo2_thmb',$this->photo2_thmb,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Leds the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
