<?php $mediaVerison=22; ?>
<?php $jsDebug=false; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no">
	<meta name="yandex-verification" content="6e99f1a849edf81a" />
	<meta name="google-site-verification" content="WrXh2CvyObUoqicNoDiboaJLLIOW9DBOZjsf7Rlr1Ks" /> 
	<meta name="description" content="Светодиодная подсветка. Светодиодная лента интерьерная. Светодиодные модули. Низкие цены и быстрая доставка">
	<meta name="keywords" content="светодиодная лента, купить светодиодную ленту, RGB лент, светодиодная лента теплая белая, led лента, smd лента, светодиодная подсветка">
	
	<link rel="stylesheet" type="text/css" href="/css/reset.css">
	<link rel="stylesheet" type="text/css" href="/css/fonts.css">
	<link rel="stylesheet" type="text/css" href="/css/style.css?v=<?php echo $mediaVerison ?>" >

	<title>Светодиодная лента оптом и в розницу, подсветка LED, модули. Продаем по стоковым ценам с гарантией!</title>
	<link rel="shortcut icon" href="/img/favicon.ico">
	<link rel="stylesheet" type="text/css" href="/js/libs/lightbox/css/lightbox.min.css">
	<script type="text/javascript" src="/js/dist/libs.js?v=<?php echo $mediaVerison ?>"></script>	
	<script type="text/javascript" src="/js/dist/build<?php if ($jsDebug) echo "-debug"?>.js?v=<?php echo $mediaVerison ?>"></script>
</head>
<body>
	<div class="mobile-menu">
		<ul class="mobile-menu__list">
			<li class="mobile-menu__list-item">
				<a href="/" class="mobile-menu__list-item-link">Главная</a>
			</li>
			<li class="mobile-menu__list-item">
				<a href="/catalog" class="mobile-menu__list-item-link">Каталог</a>				
			</li>
			<li class="mobile-menu__list-item">
				<a href="/contacts" class="mobile-menu__list-item-link">Контакты</a>
			</li>
		</ul>
	</div>
	<div class="sitewrap">
		
		<div class="mobile-menu-btn" onclick="window.mMenu.show()"></div>
		<div class="header">
			<div class="header__inner">
				<a class="header__logo" href="/">
					<img class="header__logo-img" src="img/header/logo.png">
				</a>
				<div class="header-menu">
					<?php if ($this->activeMenuLink == "index") $linkClass="header-menu__item_active"; else $linkClass="";?>
					<a href="/" class="header-menu__item <?php echo $linkClass?>">Главная</a>
					<?php if ($this->activeMenuLink == "catalog") $linkClass="header-menu__item_active"; else $linkClass="";?>
					<a href="/catalog" class="header-menu__item <?php echo $linkClass?>">Каталог продукции</a>
					<?php if ($this->activeMenuLink == "contacts") $linkClass="header-menu__item_active"; else $linkClass="";?>
					<a href="/contacts" class="header-menu__item <?php echo $linkClass?>">Контакты</a>
				</div>
				<div class="header-contacts">
					<img class="header-contacts__img" src="img/header/lamp.png">
					<div class="header-contacts__text1">8-950-749-34-64</div>
					<div class="header-contacts__text1">info@led-stok.ru</div>
					<div class="header-contacts__text2">г.Челябинск, ул. 40 лет Октября, 33</div>
				</div>
			</div>
		</div>

		<?php echo $content; ?>

		<div class="footer">
			<div class="footer__inner">
				<a class="footer__logo" href="/">
					<img class="footer__logo-img" src="img/footer/logo.png">
				</a>
				<div class="footer-info">
					<a  class="footer-info__text" href="tel:89507493464">8-950-749-34-64</a>
					<div class="footer-info__text footer-info__text_marginleft">
						<a href="mailto:infoxxx@led-stok.ru" class="footer-info__mail-link" onmouseover="this.href=this.href.replace(/x/g,'');"></a>
					</div>
					<div class="footer-info__text2">г.Челябинск, ул. 40 лет Октября, 33</div>
				</div>
				<div class="footer__year">© 2017 г.</div>
			</div>
		</div>

		<div class="order-popup">
			<div class="order-popup__inputs">
				<div class="order-popup__prod">
					<div class="order-popup__prod-img-wr">	
						<img src="" class="order-popup__prod-img">
					</div>			
					<div class="order-popup__prod-name productName"></div>
				</div>
				<div class="order-popup__inp formWrapName">
					<div class="order-popup__inp-label">Имя <span class="order-popup__inp-req formError">обязательное поле</span></div>
					<input type="text" class="order-popup__inp-val formInput">
				</div>
				<div class="order-popup__inp formWrapPhone">
					<div class="order-popup__inp-label">Телефон <span class="order-popup__inp-req formError">обязательное поле</span></div>
					<input type="text" class="order-popup__inp-val formInput">
				</div>
				<div class="order-popup__inp formWrapEmail">
					<div class="order-popup__inp-label">Email <span class="order-popup__inp-req formError">обязательное поле</span></div>
					<input type="text" class="order-popup__inp-val formInput">
				</div>
				<div class="order-popup__inp order-popup__inp_action formWrapConfirm">
					<div class="order-popup__confirmCehck-wrap ">
						<input type="checkbox" id="order-popup__confirmCehck" class="order-popup__confirmCehck formInput">
						<label for="order-popup__confirmCehck" class="order-popup__confirmCehck-label">Даю согласие на обработку <a href="/" class="order-popup__confirmCehck-link">пересональных данных</a></label>
					</div>
					<div class="order-popup__sendBtn formSendBtn">Отправить</div>
					<div class="order-popup__inp-req order-popup__inp-req_confirm">Необходимо дать согласие на обработку персональных данных</div>
				</div>
			</div>
			<div class="order-popup__sending">
				<img src="/img/loading.gif" class="order-popup__sending-img">
			</div>
			<div class="order-popup__success">
				<div class="order-popup__success-text">Сообщение отправлено!</div>
				<div class="order-popup__closeBtn" onclick="window.orderPopup.close()">Закрыть</div>
			</div>
			<div class="order-popup__fail">
				<div class="order-popup__fail-text">Произошла ошибка. Сообщение не доставлено.</div>
				<div class="order-popup__closeBtn" onclick="window.orderPopup.close()">Закрыть</div>
			</div>		
		</div>
		<div class="order-popup__bg" onclick="window.orderPopup.close()"></div>
		
	</div>
</body>
</html>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter45017725 = new Ya.Metrika({
                    id:45017725,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });
 
        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";
 
        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/45017725" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->