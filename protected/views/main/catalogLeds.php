<div class="catalog catalog_leds">
	<div class="catalog__title"><?php echo $catalogTitle; ?></div>	

	<div class="catalog-mobile-sort">
		<div class="catalog-mobile-sort__label">Сортировка</div>
		<select class="catalog-mobile-sort__select-clm">
			<option value="0">Наименование</option>
			<option value="2">Led/м</option>
			<option value="3">Защита</option>
			<option value="4">Мощность</option>
			<option value="5">Цвет</option>
			<option value="6">Цена</option>						
		</select>
		<select class="catalog-mobile-sort__orderdir-clm">
			<option value="asc" selected="selected">по возрастанию</option>
			<option value="desc">по убыванию</option>			
		</select>
	</div>

	<div class="catalog__fixed-head">
		<div class="catalog__fixed-head-inner">
			<div class="catalog-desktop-header">
				<table class="catalog-table">
					<thead class="catalog-table__thead">
						<tr class="catalog-table__tr catalog-table__tr_head">
							<td class="catalog-table__td catalog-table__td_h-name">
								<div class="catalog-table__item catalog-table__item_name-size  catalog-table__item_title" >Наименование</div>
							</td>
							<td class="catalog-table__td catalog-table__td_h-catalog">
								<div class="catalog-table__item catalog-table__item_catalog-size catalog-table__item_title"></a>
							</td>
							<td class="catalog-table__td catalog-table__td_h-density">
								<div class="catalog-table__item catalog-table__item_density-size catalog-table__item_title">led/м</div>
							</td>
							<td class="catalog-table__td catalog-table__td_h-protection">
								<div class="catalog-table__item catalog-table__item_protection-size catalog-table__item_title">защита</div>
							</td>
							<td class="catalog-table__td catalog-table__td_h-power">
								<div class="catalog-table__item catalog-table__item_power-size catalog-table__item_title" >Ватт/м</div>
							</td>
							<td class="catalog-table__td catalog-table__td_h-color">
								<div class="catalog-table__item catalog-table__item_color-size catalog-table__item_title" >Цвет</div>
							</td>
							<td class="catalog-table__td catalog-table__td_h-price">
								<div class="catalog-table__item catalog-table__item_price-size catalog-table__item_title" >Цена</div>
							</td>
							<td class="catalog-table__td catalog-table__td_h-stock">
								<div class="catalog-table__item catalog-table__item_stock-size catalog-table__item_title">
									
								</div>
							</td>
							<td class="catalog-table__td catalog-table__td-img">
								<div class="catalog-table__item catalog-table__item_img-wrap-size catalog-table__item_title">
									
								</div>						
							</td>
							<td class="catalog-table__td">							
								<div class="catalog-table__item catalog-table__item_orderBtn-size catalog-table__item_title"></div>
							</td>
						</tr>
					</thead>
				</table>
			</div>

			<div class="catalog-mobile-header">
								
			</div>

		</div> <!-- end catalog__fixed-head-inner -->
	</div> <!-- end catalog__fixed-head -->


	<div class="catalog__table-wrap">
		<table id="catalog-table" class="catalog-table">
			<thead style="display:none">
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
			</thead>	
			<tbody class="catalog-table__tbody">	
				<?php foreach ($products as $product): ?>			
					<tr class="catalog-table__tr catalog-table__tr_body">
						<td class="catalog-table__td catalog-table__td_name">
							<div class="catalog-table__item catalog-table__item_name catalog-table__item_name-size" ><?php echo $product->name ?></div>
						</td>
						<td class="catalog-table__td">
							<?php if ($product->photo3!=null): ?>
								<a href="<?php echo $product->photo3Url ?>" class="catalog-table__item catalog-table__item_catalog catalog-table__item_catalog-size" data-lightbox="prod<?php echo $product->id ?>" data-title="<?php echo $product->name ?>">каталог</a>
							<?php endif; ?>
						</td>
						<td class="catalog-table__td">
							<div class="catalog-table__item catalog-table__item_density catalog-table__item_density-size" ><?php echo $product->density ?></div>
						</td>
						<td class="catalog-table__td">
							<div class="catalog-table__item catalog-table__item_protection catalog-table__item_protection-size" ><?php echo $product->protection ?></div>
						</td>
						<td class="catalog-table__td">
							<div class="catalog-table__item catalog-table__item_power catalog-table__item_power-size" ><?php echo $product->power ?></div>
						</td>
						<td class="catalog-table__td catalog-table__td_color">
							<div class="catalog-table__item catalog-table__item_color catalog-table__item_color-size" ><?php echo $product->color ?></div>
						</td>
						<td class="catalog-table__td">
							<div class="catalog-table__item catalog-table__item_price catalog-table__item_price-size" ><?php echo $product->price ?></div>
						</td>
						<td class="catalog-table__td">
							<div class="catalog-table__item catalog-table__item_stock catalog-table__item_stock-size">
								<?php if ($product->stock==0):?>
									нет в наличии
								<?php else: ?>
									в наличии
								<?php endif; ?>
							</div>
						</td>
						<td class="catalog-table__td catalog-table__td-img">
							<div class="catalog-table__item catalog-table__item_img-wrap catalog-table__item_img-wrap-size">
								<?php if ($product->photo1!=null): ?>
								<a href="<?php echo $product->photo1Url ?>" class="catalog-table__item-img1-link" data-lightbox="prod<?php echo $product->id ?>" data-title="<?php echo $product->name ?>">
									<img src="<?php echo $product->photo1ThmbUrl ?>" class="catalog-table__item-img1">
								</a>
								<?php endif; ?>
								<?php if ($product->photo2!=null): ?>
								<a href="<?php echo $product->photo2Url ?>" class="catalog-table__item-img2-link" data-lightbox="prod<?php echo $product->id ?>" data-title="<?php echo $product->name ?>">
									<img src="<?php echo $product->photo2ThmbUrl ?>" class="catalog-table__item-img2">
								</a>
								<?php endif; ?>
							</div>						
						</td>
						<td class="catalog-table__td">							
							<div class="catalog-table__item catalog-table__item_orderBtn catalog-table__item_orderBtn-size" onclick="window.orderPopup.show('<?php echo $product->name ?>','<?php echo $product->photo2ThmbUrl ?>')">заказать</div>
						</td>
					</tr>
				<? endforeach; ?>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	window.catalogSearch = <?php echo CJavaScript::encode($search)?>;
</script>