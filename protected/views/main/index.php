<div class="slider">
	<div class="slide slide_1">
		<div class="slide__text slide__text_1">Светодиодные ленты<br> по доступным ценам</div>
	</div>
	<div class="slide slide_2">
		<div class="slide__text slide__text_2">Светодиодные ленты<br> по стоковым ценам</div>
	</div>
</div>

<div class="maincat">
	<div class="maincat-inner">
		<div class="maincat__head">
			<div class="maincat__title">Светодиодные ленты</div>
			
			<form class="maincat__input" action="/catalog" >
				<input type="text" name="search" placeholder="Поиск по каталогу" class="maincat__input-element">
				<input type="submit" value="" class="maincat__input-button">
			</form>
		</div>	
		<div class="maincat-items">
				<?php $this->renderPartial('index/ledsList',array('products'=>$products)) ?>
		</div>
		
		<a href="/catalog" class="maincat__button">Смотреть весь каталог</a>
	</div>
</div>



<div class="main-about">
	<p class="main-about__text">Светодиодная лента - это наиболее современный источник света, с огромными областями применения: освещение помещений, декоративная подсветка зданий, оформление мероприятий, витрин, в рекламе. Светодиоды обладают высокой яркостью, долгим сроком службы, низким энергопотреблением, и высокой энергоэффективностью. Светодиодный свет - это современный и эффективный источник, с помощью которого можно превратить любое помещение в дизайнерскую находку.</p>
	<p class="main-about__text">Иначе светодиодную ленту еще называют LED лента – это гибкая печатная плата с токоведущими дорожками, на лицевой стороне которой припаяны светодиоды и элементы, обеспечивающие их работу. На тыльной стороне, чаще всего, присутствует клейкая лента для приклеивания светодиодной ленты к поверхности. Ленты обладают различными характеристиками, их различают по яркости и цветовой гамме, количеству диодов, цветам, мощности, исполнению.</p>
	<p class="main-about__text">Благодаря своим уникальным свойствам, область применения светодиодной ленты ограничивается только Вашей фантазией.</p>
	
	<h3 class="main-about__list-title">Атрибуты надежной светодиодной ленты:</h3>
	<ul class="main-about__list">
		<li class="main-about__list-item">- Изготавливается на большом заводе от кристалла до упаковки, контролируется на каждом этапе</li>
		<li class="main-about__list-item">- Применяется качественный люминофор и аноды</li>
		<li class="main-about__list-item">- Светодиоды подбираются с одинаковыми характеристиками</li>
		<li class="main-about__list-item">- Лента достаточно толстая, выдерживающая большие токи</li>
	</ul>

	<h3 class="main-about__list-title">Виды светодиодных лент:</h3>
	<ul class="main-about__list">
		<li class="main-about__list-item">- SMD 3528 60, 120 диодов/метр</li>
		<li class="main-about__list-item">- SMD 2835 60, 120 диодов/метр</li>
		<li class="main-about__list-item">- SMD 5050 30, 60 диодов/метр</li>
		<li class="main-about__list-item">- SMD 5630 60 диодов/метр</li>
		<li class="main-about__list-item">- SMD 5730 60 диодов/метр</li>
		<li class="main-about__list-item">- SMD 3014 60, 120 диодов/метр</li>
	</ul>

	<h3 class="main-about__list-title">Наиболее часто встречающиеся цвета свечения светодиодных лент:</h3>
	<ul class="main-about__list">
		<li class="main-about__list-item"><span class="main-about__colorname">Red</span> - красный 625 нм</li>
		<li class="main-about__list-item"><span class="main-about__colorname">Orange</span> - оранжевый 610 нм</li>
		<li class="main-about__list-item"><span class="main-about__colorname">Yellow</span> - жёлтый 590 нм</li>
		<li class="main-about__list-item"><span class="main-about__colorname">Green</span> - зелёный 525 нм</li>
		<li class="main-about__list-item"><span class="main-about__colorname">Blue</span> - синий 470 нм</li>
		<li class="main-about__list-item"><span class="main-about__colorname">Cool White</span> - белый холодный 8000-9000 К</li>
		<li class="main-about__list-item"><span class="main-about__colorname">Day White</span> - белый дневной 3800-4200 К</li>
		<li class="main-about__list-item"><span class="main-about__colorname">Warm white</span> - белый тёплый 2700-2900 К</li>
		<li class="main-about__list-item"><span class="main-about__colorname">Violet</span> - ультрафиолет 400 нм, видимый, не направлять в глаза</li>
		<li class="main-about__list-item"><span class="main-about__colorname">Pink</span> - розовый</li>
		<li class="main-about__list-item"><span class="main-about__colorname">RGB</span> - мультицветная лента, 3 канала</li>
		<li class="main-about__list-item"><span class="main-about__colorname">RGBW</span> - мультицветная лента с белым LED, 4 канала</li>
	</ul>

	<h3 class="main-about__list-title">Применение светодиодной ленты:</h3>
	<ul class="main-about__list">
		<li class="main-about__list-item">- подсветка потолков, ниш, стен, лестниц</li>
		<li class="main-about__list-item">- освещение рабочих зон кухни</li>
		<li class="main-about__list-item">- подсветка натяжных потолков</li>
		<li class="main-about__list-item">- регулируемая интерьерная подсветка</li>
		<li class="main-about__list-item">- подсветка рекламных боксов, букв, вывесок и продукции</li>
		<li class="main-about__list-item">- создание спецэффектов в интерьере</li>
		<li class="main-about__list-item">- светильники на основе алюминиевого профиля</li>
		<li class="main-about__list-item">- подсветка шкафов и полок</li>
		<li class="main-about__list-item">- освещение жилых и офисных помещений</li>
		<li class="main-about__list-item">- подсветка ювелирных витрин</li>
		<li class="main-about__list-item">- художественная подсветка арт-объектов</li>
		<li class="main-about__list-item">- световое оформление клубов, ресторанов, витрин магазинов</li>
	</ul>

	<h3 class="main-about__list-title">Характеристики интерьерных светодиодных лент:</h3>
	<ul class="main-about__list">
		<li class="main-about__list-item">- напряжение питания 12 или 24 вольт</li>
		<li class="main-about__list-item">- мощность 2,1–24 Вт/метр</li>
		<li class="main-about__list-item">- энергосбережение до 90%, по сравнению с лампами накаливания</li>
		<li class="main-about__list-item">- длительный срок службы 30000 … 50000 часов работы</li>
		<li class="main-about__list-item">- плотность светодиодов – 30…240 светодиодов на метр</li>
		<li class="main-about__list-item">- большое разнообразие цветов и оттенков</li>
		<li class="main-about__list-item">- простое и удобное управления яркостью и цветом свечения</li>
		<li class="main-about__list-item">- удобство и безопасность монтажа</li>
		<li class="main-about__list-item">- различная степень герметизации IP20…IP67</li>
		<li class="main-about__list-item">- отсутствие в составе ленты ртути и других вредных веществ</li>
	</ul>
</div>